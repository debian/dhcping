dhcping (1.2-6) unstable; urgency=medium

  * New maintainer (Closes: #934420)
  * Bump standards to 4.6.2, no changes
  * Mark patches as forwarded upstream (by email)
  * Add lintian overrides
  * Update metadata

 -- Boian Bonev <bbonev@ipacct.com>  Wed, 21 Jun 2023 15:28:44 +0000

dhcping (1.2-5) unstable; urgency=medium

  * QA upload.
  * Switch to 3.0 (quilt) source package format:
    - Added source/format
    - Patches created for dhcping.8 and dhcping.c (split by authors and
      changeset)
    - Drop patch to update config.guess and config.sub. Now done via
      dh_update_autotools_config
  * d/control:
    - Set maintainer to Debian QA Group (Orphan bug: #934420)
    - Add Homepage
    - Add Vcs field pointing to salsa
    - Add Rules-Requires-Root: no
    - Bump Std-Version to 4.5.0
    - Add missing ${misc:Depends}
  * Remove d/NMU-Disclaimer as package is orphaned
  * Convert copyright to DEP-5
  * Convert buildsystem to dh:
    - Add debhelper-compat (= 13) as Build-Depends
    - Remove useless autotools-dev Build-Depends
    - Rewrite rules using dh
    - Skip autoreconf due to missing mandatory files
    - Override dh_auto_configure to skip unsupported runstatedir option
    - Specify host when cross-building (Closes: #870647)
  * Delegate install to debhelper:
    - Skip Makefile install rule (wrong installation directory)
    - Add files to d/install
    - Add manpages to d/manpages
  * Drop unused d/postinst and d/prerm
  * Add watch file (no signature)
  * Add salsa CI pipeline
  * Remove trailing space from changelog
  * Add DEP-12 metadata file
  * Use recommended branch name from DEP-14
  * Fix groff warning due to older ''' module in manpage

 -- Baptiste BEAUPLAT <lyknode@cilg.org>  Fri, 28 Aug 2020 16:50:10 +0200

dhcping (1.2-4.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Make the build reproducible. (Closes: #777320)

 -- Chris Lamb <lamby@debian.org>  Sun, 20 Aug 2017 10:32:20 -0700

dhcping (1.2-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update config.{guess,sub} to support new architectures (Closes: #533687)

 -- Chen Baozi <baozich@gmail.com>  Tue, 03 Jun 2014 11:04:13 +0800

dhcping (1.2-4) unstable; urgency=low

  * Fixed typo in NMU-Disclaimer, thanks to Petter Reinholdtsen (See:
    Bug#225895)
  * Added support for nostrip (closes: Bug#436760)
  * Added support for debug option
  * Document -V as extremely verbose option (closes: Bug#364694)
  * Compress changelog.Debian even if it's very small (closes: Bug#355687)
  * Fixed typos by A Costa <agcosta@gis.net> (closes: Bug#367154)

 -- Joey Schulze <joey@infodrom.org>  Sat, 28 Jun 2008 18:55:45 +0200

dhcping (1.2-3) unstable; urgency=medium

  * Applied patch by Henrik Stoerner to fix infinite loop when processing
    commandline arguments (closes: Bug#327083)
  * Updated config.guess and config.sub from autotools-dev 20060223.1 to
    update architectures (closes: Bug#342404)
  * Applied spelling patch by A Costa (closes: Bug#334476)
  * Corrected boldface in the SEE ALSO section
  * Disabled installation of postinst and prerm scripts
  * Updated the Standards-Version

 -- Martin Schulze <joey@infodrom.org>  Sat,  4 Mar 2006 22:35:30 +0100

dhcping (1.2-2) unstable; urgency=medium

  * Added a note about the packages' cvs directory
  * Added special detection routine for big/little endianess on MIPS since
    the line "byteorder : {big|little} endian" from /proc/cpuinfo was
    removed as of Linux 2.4.20, resulting in the mipsel buildd being
    unable to build this package. (closes: Bug#274504)

 -- Martin Schulze <joey@infodrom.org>  Sun,  7 Nov 2004 16:10:38 +0100

dhcping (1.2-1) unstable; urgency=low

  * Added a disclaimer for those people who plan to NMU this package
  * New upstream version (132924)
  * Added support for auto-configure script
  * Corrected clean target
  * Corrected bold/italics in manpage

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Tue, 12 Feb 2002 10:13:17 +0100

dhcping (1.1-2) unstable; urgency=low

  * Whoops, forgot to add the postinst and prerm scripts
  * Added -isp to dpkg-gencontrol

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sat,  8 Dec 2001 15:17:18 +0100

dhcping (1.1-1) unstable; urgency=low

  * Initial version

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sun, 18 Nov 2001 20:10:19 +0100
